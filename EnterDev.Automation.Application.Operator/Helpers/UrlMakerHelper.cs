﻿using EnterDev.Automation.Domain.Entities.Model;
using EnterDev.Automation.Domain.Interfaces.Application;
using System.Configuration;

namespace EnterDev.Automation.Application.Operator.Helpers
{
    public class UrlMakerHelper : IUrlMakerHelper
    {
        public UrlMakerHelper()
        {

        }

        public string GetUrlVerifyCompany()
        {
            string verifyUrl = ConfigurationManager.AppSettings["verifyUrl"] ?? "";
            string machine = ConfigurationManager.AppSettings["machine"] ?? "";
            string domain = ConfigurationManager.AppSettings["domain"] ?? "";
            string username = ConfigurationManager.AppSettings["username"] ?? "";
            string format = ConfigurationManager.AppSettings["format"] ?? "";

            return string.Format(verifyUrl, machine, domain, username, format);
        }

        public string GetUrlFromDownloadFile()
        {
            return ConfigurationManager.AppSettings["downloadUrl"];
        }

        public string GetUrlToDownloadFile()
        {
            return ConfigurationManager.AppSettings["localPathToDownload"];
        }

        public string GerUrlAuthenticationService()
        {
            return ConfigurationManager.AppSettings["authenticationUrl"];
        }

        public ServiceParameterModel[] GetAuthenticationParameters()
        {
            ServiceParameterModel[] parameters = new ServiceParameterModel[3];

            parameters[0] = new ServiceParameterModel("username", ConfigurationManager.AppSettings["authenticationUserName"]);
            parameters[1] = new ServiceParameterModel("password", ConfigurationManager.AppSettings["authenticationPassword"]);
            parameters[2] = new ServiceParameterModel("autos", ConfigurationManager.AppSettings["authenticationAutos"]);

            return parameters;
        }

        public string GetAutoDataUrl(string idAuto)
        {
            return string.Format(ConfigurationManager.AppSettings["autoDataUrl"], idAuto);
        }
    }
}
