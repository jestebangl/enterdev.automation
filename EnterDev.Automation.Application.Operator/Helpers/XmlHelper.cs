﻿namespace EnterDev.Automation.Application.Operator.Helpers
{
    using EnterDev.Automation.Domain.Entities.Enum;
    using EnterDev.Automation.Domain.Entities.Helper;
    using EnterDev.Automation.Domain.Entities.Model;
    using EnterDev.Automation.Domain.Interfaces.Application;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    public class XmlHelper : IXmlHelper
    {
        public XmlHelper()
        {

        }

        public XDocument GetXDocumentoFromString(string xmlString)
        {
            try
            {
                return XDocument.Parse(xmlString);
            }
            catch (Exception ex)
            {
                throw new EnterDevException("GetXDocumentoFromString", ErrorMessagesEnum.XmlParseError, ex);
            }
        }

        public MachineModel GetMachineModelFromXDocument(XDocument document)
        {
            var machineModel = from row in document.Descendants("Machine").AsEnumerable()
                               select new MachineModel()
                               {
                                   cid = row.Attribute("cid").Value,
                                   id = row.Attribute("Id").Value,
                                   lastUpdate = row.Attribute("Id").Value,
                                   isTraining = row.Attribute("is_training").Value
                               };

            if (machineModel.Any())
            {
                return machineModel.FirstOrDefault();
            }

            throw new EnterDevException("GetMachineModelFromXDocument", ErrorMessagesEnum.MachineNotFound);
        }

        public List<GroupModel> GetGroupModelFromXDocument(XDocument document, MachineModel machine)
        {
            var groupModelList = from row in document.Descendants("group").AsEnumerable()
                                 where row.Parent.Parent.Parent.Attribute("cid").Value.Equals(machine.cid)
                                 select new GroupModel()
                                 {
                                     cid = row.Attribute("cid").Value,
                                     name = row.Attribute("name").Value,
                                     gid = row.Attribute("gid").Value
                                 };

            if (groupModelList.Any())
            {
                return groupModelList.ToList();
            }

            throw new EnterDevException("GetGroupModelFromXDocument", ErrorMessagesEnum.GroupDoesNotExist);
        }


        public void GetAutoModelForEachGroupModel(XDocument document, List<GroupModel> groupModelList)
        {
            foreach (var group in groupModelList)
            {
                group.autoModelList = GetAutoModelForEachGroupModel(document, group);
            }
        }

        private List<AutoModel> GetAutoModelForEachGroupModel(XDocument document, GroupModel groupModel)
        {
            var autoModelList = from row in document.Descendants("auto").AsEnumerable()
                                where row.Parent.Attribute("cid").Value.Equals(groupModel.cid) &&
                                    row.Parent.Attribute("gid").Value.Equals(groupModel.gid)
                                select new AutoModel()
                                {
                                    name = row.Attribute("name").Value,
                                    version = row.Attribute("version").Value,
                                    fileName = row.Element("file").Value,
                                    autoId = row.Element("auto_id").Value,
                                    realtime = row.Element("realtime").Value,
                                    hash = row.Element("hash").Value,
                                    hidden = row.Element("hidden").Value,
                                    key = row.Element("key").Value,
                                    url = row.Element("url").Value,
                                    secure = row.Element("secure").Value
                                };

            return autoModelList.ToList();

        }
    }
}
