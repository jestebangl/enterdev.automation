﻿namespace EnterDev.Automation.Application.Operator.Application
{
    using EnterDev.Automation.Domain.Entities.Model;
    using EnterDev.Automation.Domain.Interfaces.Application;
    using EnterDev.Automation.Domain.Interfaces.Infraestructure;
    using System.Collections.Generic;

    public class MachineApplication : IMachineApplication
    {
        public IUrlMakerHelper UrlMakerHelper { get; set; }
        public IXmlHelper XmlHelper { get; set; }
        public IRestServiceIntegration RestServiceIntegration { get; set; }

        public MachineApplication(IRestServiceIntegration RestServiceIntegration, IUrlMakerHelper UrlMakerHelper)
        {
            this.UrlMakerHelper = UrlMakerHelper;
            this.RestServiceIntegration = RestServiceIntegration;
        }

        public string ExceuteServiceToVerifyCompany()
        {
            string url = UrlMakerHelper.GetUrlVerifyCompany();
            string response = RestServiceIntegration.ExecuteGetService(url);
            return response;
        }

        public MachineModel GetMachineModelFromXml(string xmlString)
        {
            var document = XmlHelper.GetXDocumentoFromString(xmlString);

            var machine = XmlHelper.GetMachineModelFromXDocument(document);
            var groupModelList = XmlHelper.GetGroupModelFromXDocument(document, machine);

            XmlHelper.GetAutoModelForEachGroupModel(document, groupModelList);

            machine.groupModelList = groupModelList;

            return machine;
        }

        public async void DownloadAutoFiles(List<GroupModel> groupModelList)
        {
            foreach (var group in groupModelList)
            {
                foreach (var auto in group.autoModelList)
                {
                    string urlOrigin = $"{UrlMakerHelper.GetUrlFromDownloadFile()}{auto.fileName}";
                    string destinationPath = $"{UrlMakerHelper.GetUrlToDownloadFile()}{auto.autoId}_{auto.fileName}";

                    RestServiceIntegration.DownloadFile(urlOrigin, destinationPath);
                }
            }
        }
    }
}
