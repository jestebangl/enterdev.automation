﻿using EnterDev.Automation.Domain.Entities.Helper;
using EnterDev.Automation.Domain.Entities.Model;
using EnterDev.Automation.Domain.Interfaces.Application;
using EnterDev.Automation.Domain.Interfaces.Infraestructure;
using Newtonsoft.Json;

namespace EnterDev.Automation.Application.Operator.Application
{
    public class SecurityApplication : ISecurityApplication
    {
        public IUrlMakerHelper UrlMakerHelper { get; set; }
        public IRestServiceIntegration RestServiceIntegration { get; set; }

        public SecurityApplication(IUrlMakerHelper UrlMakerHelper, IRestServiceIntegration RestServiceIntegration)
        {
            this.UrlMakerHelper = UrlMakerHelper;
            this.RestServiceIntegration = RestServiceIntegration;
        }

        public SecurityModel AuthenticationService()
        {
            string url = UrlMakerHelper.GerUrlAuthenticationService();
            var response = RestServiceIntegration.ExecutePostService(url, UrlMakerHelper.GetAuthenticationParameters());
            var securityModel = JsonConvert.DeserializeObject<SecurityModel>(response);

            if (bool.Parse(securityModel.success))
            {
                return securityModel;
            }

            throw new EnterDevException(securityModel.error);
        }

        public string GetAutoData(AutoAuthorizationModel autoAuthorizationModel)
        {
            string url = UrlMakerHelper.GetAutoDataUrl(autoAuthorizationModel.autoId);
            var response = RestServiceIntegration.ExecuteGetService(url, true, true, autoAuthorizationModel.token);
            var autoResponse = JsonConvert.DeserializeObject<AutoResponseAuthenticationModel>(response);

            if (autoResponse.success)
            {
                return autoResponse.data;
            }

            throw new EnterDevException("GetAutoData", autoResponse.code);
        }
    }
}
