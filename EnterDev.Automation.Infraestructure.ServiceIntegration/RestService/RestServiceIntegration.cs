﻿namespace EnterDev.Automation.Infraestructure.ServiceIntegration.RestService
{
    using EnterDev.Automation.Domain.Entities.Enum;
    using EnterDev.Automation.Domain.Entities.Helper;
    using EnterDev.Automation.Domain.Entities.Model;
    using EnterDev.Automation.Domain.Interfaces.Infraestructure;
    using RestSharp;
    using System;
    using System.Net;

    public class RestServiceIntegration : IRestServiceIntegration
    {
        public RestServiceIntegration() { }

        public string ExecuteGetService(string url, bool isJson = false, bool hasAuthotization = false, string tokenAuthorization = null)
        {
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                if (isJson)
                {
                    request.AddHeader("Content-Type", "application/json");
                }

                if (hasAuthotization)
                {
                    request.AddHeader("X-Authorization", $"Bearer {tokenAuthorization}");
                }

                IRestResponse response = client.Execute(request);
                return response.Content;
            }
            catch (Exception ex)
            {
                throw new EnterDevException("ExceuteGetService", ErrorMessagesEnum.ExecuteServiceException, ex);
            }
        }

        public void DownloadFile(string urlService, string localPathToDownload)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(urlService, localPathToDownload);
            }
        }

        public string ExecutePostService(string url, params ServiceParameterModel[] serviceParameters)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            foreach (var param in serviceParameters)
            {
                request.AddParameter(param.name, param.value);
            }

            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}
