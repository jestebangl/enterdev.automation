﻿using EnterDev.Automation.Domain.Entities.Model;

namespace EnterDev.Automation.Domain.Interfaces.Service
{
    public interface IOrchestratorService
    {
        MachineModel GetMachineFromService();
    }
}
 