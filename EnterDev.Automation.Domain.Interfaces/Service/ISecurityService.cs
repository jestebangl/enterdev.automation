﻿namespace EnterDev.Automation.Domain.Interfaces.Service
{
    using EnterDev.Automation.Domain.Entities.Model;

    public interface ISecurityService
    {
        SecurityModel ExecuteAuthenticationService();

        string GetAutoData(AutoAuthorizationModel autoAuthorizationModel);
    }
}
