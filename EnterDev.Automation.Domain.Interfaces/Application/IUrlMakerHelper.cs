﻿namespace EnterDev.Automation.Domain.Interfaces.Application
{
    using EnterDev.Automation.Domain.Entities.Model;


    public interface IUrlMakerHelper
    {
        string GetUrlVerifyCompany();

        string GetUrlFromDownloadFile();

        string GetUrlToDownloadFile();

        string GerUrlAuthenticationService();

        ServiceParameterModel[] GetAuthenticationParameters();

        string GetAutoDataUrl(string idAuto);
    }
}
