﻿namespace EnterDev.Automation.Domain.Interfaces.Application
{
    using EnterDev.Automation.Domain.Entities.Model;
    using System.Collections.Generic;

    public interface IMachineApplication
    {
        string ExceuteServiceToVerifyCompany();

        MachineModel GetMachineModelFromXml(string xmlString);

        void DownloadAutoFiles(List<GroupModel> groupModelList);
    }
}
