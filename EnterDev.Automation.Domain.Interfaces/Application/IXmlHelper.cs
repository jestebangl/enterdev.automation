﻿namespace EnterDev.Automation.Domain.Interfaces.Application
{
    using EnterDev.Automation.Domain.Entities.Model;
    using System.Collections.Generic;
    using System.Xml.Linq;


    public interface IXmlHelper
    {
        XDocument GetXDocumentoFromString(string xmlString);

        MachineModel GetMachineModelFromXDocument(XDocument document);

        List<GroupModel> GetGroupModelFromXDocument(XDocument document, MachineModel machine);

        void GetAutoModelForEachGroupModel(XDocument document, List<GroupModel> groupModelList);
    }
}
