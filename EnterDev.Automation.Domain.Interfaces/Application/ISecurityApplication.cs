﻿using EnterDev.Automation.Domain.Entities.Model;

namespace EnterDev.Automation.Domain.Interfaces.Application
{
    public interface ISecurityApplication
    {
        SecurityModel AuthenticationService();

        string GetAutoData(AutoAuthorizationModel autoAuthorizationModel);
    }
}
