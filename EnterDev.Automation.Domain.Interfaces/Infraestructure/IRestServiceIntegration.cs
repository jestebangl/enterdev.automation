﻿namespace EnterDev.Automation.Domain.Interfaces.Infraestructure
{
    using EnterDev.Automation.Domain.Entities.Model;

    public interface IRestServiceIntegration
    {
        string ExecuteGetService(string url, bool isJson = false, bool hasAuthotization = false, string tokenAuthorization = null);

        void DownloadFile(string urlService, string localPathToDownload);

        string ExecutePostService(string url, params ServiceParameterModel[] serviceParameters);
    }
}
