﻿namespace EnterDev.Automation.Domain.Entities.Helper
{
    using EnterDev.Automation.Domain.Entities.Enum;
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class EnterDevException : Exception
    {
        public EnterDevException() : base() { }

        public EnterDevException(string message) : base(message)
        {
        }

        public EnterDevException(string methodException, ErrorMessagesEnum userExceptionMessage, Exception ex = null)
        {
            if (ex != null)
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {ex.ToString()}");
            }
            else
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {userExceptionMessage.ToDescriptionString()}");
            }

            throw new EnterDevException(userExceptionMessage.ToDescriptionString());
        }


        public EnterDevException(string methodException, ErrorAgilityApiEnum userExceptionMessage, Exception ex = null)
        {
            if (ex != null)
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {ex.ToString()}");
            }
            else
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {userExceptionMessage.ToDescriptionString()}");
            }

            throw new EnterDevException(userExceptionMessage.ToDescriptionString());
        }

        protected EnterDevException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
