﻿namespace EnterDev.Automation.Domain.Entities.Enum
{
    using System.ComponentModel;


    public enum ErrorAgilityApiEnum
    {
        [Description("El envió de parámetros debe ser via GET")]
        GetParameter = 001,
        [Description("No se ha enviado en el header, el parámetro X-Authorization con su valor")]
        HeaderNotFound = 002,
        [Description("Token expirado")]
        InvalidToken = 003,
        [Description("Problemas en la autenticación del usuario")]
        AuthenticationError = 004
    }

    public static partial class ErrorAgilityApiEnumExtention
    {
        public static string ToDescriptionString(this ErrorAgilityApiEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
