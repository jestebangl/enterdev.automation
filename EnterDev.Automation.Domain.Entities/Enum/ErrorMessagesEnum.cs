﻿namespace EnterDev.Automation.Domain.Entities.Enum
{
    using System.ComponentModel;


    public enum ErrorMessagesEnum
    {
        [Description("Ocurrio un error consultando el servicio de 'api/verificator/VerifComp'.")]
        ExecuteServiceException = 1,
        [Description("Ocurrio error inesperado en la aplicación, porfavor intentelo de nuevo.")]
        GenericError = 2,
        [Description("En el documento XML no contiene 'group'.")]
        GroupDoesNotExist = 3,
        [Description("Ocurrio un error convirtiendo la respuesta del servicio REST a XML.")]
        XmlParseError = 4,
        [Description("La maquina especificada no fue encontrada.")]
        MachineNotFound = 5,
        [Description("Ocurrio un error en el servicio de autenticación.")]
        AuthenticationServiceError = 6


    }

    public static partial class ErrorMessageEnumExtention
    {
        public static string ToDescriptionString(this ErrorMessagesEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
