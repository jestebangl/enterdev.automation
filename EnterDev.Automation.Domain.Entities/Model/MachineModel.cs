﻿namespace EnterDev.Automation.Domain.Entities.Model
{
    using System;
    using System.Collections.Generic;

    public class MachineModel
    {
        public string id { get; set; }

        public string isTraining { get; set; }

        public string lastUpdate { get; set; }

        public string cid { get; set; }

        public List<GroupModel> groupModelList { get; set; }

        public MachineModel()
        {

        }
    }
}
