﻿namespace EnterDev.Automation.Domain.Entities.Model
{
    using System.Collections.Generic;

    public class SecurityModel
    {
        public string success { get; set; }

        public string error { get; set; }

        public string id { get; set; }

        public string name { get; set; }

        public string owner { get; set; }

        public string token { get; set; }

        public string perms { get; set; }

        public List<AutoModel> autos { get; set; }

        public SecurityModel()
        {
        }
    }
}
