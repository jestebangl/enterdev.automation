﻿namespace EnterDev.Automation.Domain.Entities.Model
{
    public partial class AutoModel
    {
        public string name { get; set; }

        public string version { get; set; }

        public string fileName { get; set; }

        public string autoId { get; set; }

        public string realtime { get; set; }

        public string hash { get; set; }

        public string hidden { get; set; }

        public string key { get; set; }

        public string url { get; set; }

        public string secure { get; set; }

        public AutoModel() { }
    }

    public partial class AutoModel
    {
        public string id { get; set; }

        public string automation_file { get; set; }
    }
}
