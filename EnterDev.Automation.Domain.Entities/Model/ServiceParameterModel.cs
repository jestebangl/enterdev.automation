﻿namespace EnterDev.Automation.Domain.Entities.Model
{
    public class ServiceParameterModel
    {
        public string name { get; set; }
        public string value { get; set; }

        public ServiceParameterModel() { }

        public ServiceParameterModel(string name, string value)
        {
            this.name = name;
            this.value = value;
        }
    }
}
