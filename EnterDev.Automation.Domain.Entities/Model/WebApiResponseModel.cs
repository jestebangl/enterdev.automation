﻿namespace EnterDev.Automation.Domain.Entities.Model
{
    using System.Collections.Generic;

    public class WebApiResponseModel
    {
        public List<string> Errors { get; set; }

        public object Resultado { get; set; }

        public bool EsExitoso { get; set; }

        public WebApiResponseModel(object resultado, List<string> errors, bool esExitoso)
        {
            Errors = errors;
            Resultado = resultado;
            EsExitoso = esExitoso;
        }
    }
}
