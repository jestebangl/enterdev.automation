﻿namespace EnterDev.Automation.Domain.Entities.Model
{
    public class AutoAuthorizationModel
    {
        public string autoId { get; set; }

        public string token { get; set; }

        public AutoAuthorizationModel() { }
    }
}
