﻿namespace EnterDev.Automation.Domain.Entities.Model
{
    using EnterDev.Automation.Domain.Entities.Enum;

    public class AutoResponseAuthenticationModel
    {
        public bool success { get; set; }

        public string error { get; set; }

        public ErrorAgilityApiEnum code { get; set; }

        public string data { get; set; }

        public AutoResponseAuthenticationModel() { }
    }
}
