﻿namespace EnterDev.Automation.Domain.Entities.Model
{
    using System.Collections.Generic;

    public class GroupModel
    {
        public string name { get; set; }

        public string cid { get; set; }

        public string gid { get; set; }

        public List<AutoModel> autoModelList { get; set; }

        public GroupModel() { }
    }
}
