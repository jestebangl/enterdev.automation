﻿namespace EnterDev.Automation.Service.Orchestrator
{
    using EnterDev.Automation.Domain.Entities.Enum;
    using EnterDev.Automation.Domain.Entities.Helper;
    using EnterDev.Automation.Domain.Entities.Model;
    using EnterDev.Automation.Domain.Interfaces.Application;
    using EnterDev.Automation.Domain.Interfaces.Service;
    using System;

    public class SecurityService : ISecurityService
    {
        public ISecurityApplication SecurityApplication { get; set; }

        public SecurityService(ISecurityApplication SecurityApplication)
        {
            this.SecurityApplication = SecurityApplication;
        }

        public SecurityModel ExecuteAuthenticationService()
        {
            try
            {
                return SecurityApplication.AuthenticationService();
            }
            catch (EnterDevException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new EnterDevException("ExecuteAuthenticationService", ErrorMessagesEnum.AuthenticationServiceError, ex);
            }
        }

        public string GetAutoData(AutoAuthorizationModel autoAuthorizationModel)
        {
            try
            {
                return SecurityApplication.GetAutoData(autoAuthorizationModel);
            }
            catch (EnterDevException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new EnterDevException("ExecuteAuthenticationService", ErrorMessagesEnum.AuthenticationServiceError, ex);
            }
        }
    }
}
