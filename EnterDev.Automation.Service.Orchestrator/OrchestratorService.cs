﻿namespace EnterDev.Automation.Service.Orchestrator
{
    using EnterDev.Automation.Domain.Entities.Enum;
    using EnterDev.Automation.Domain.Entities.Helper;
    using EnterDev.Automation.Domain.Entities.Model;
    using EnterDev.Automation.Domain.Interfaces.Application;
    using EnterDev.Automation.Domain.Interfaces.Service;
    using System;
    using System.Threading.Tasks;

    public class OrchestratorService : IOrchestratorService
    {
        public IMachineApplication MachineApplication { get; set; }

        public OrchestratorService()
        {

        }

        public MachineModel GetMachineFromService()
        {
            try
            {
                string xmlString = MachineApplication.ExceuteServiceToVerifyCompany();
                var machineModel = MachineApplication.GetMachineModelFromXml(xmlString);

                Task.Run(() => MachineApplication.DownloadAutoFiles(machineModel.groupModelList));

                return machineModel;

            }
            catch (EnterDevException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new EnterDevException("GetGroupModelFromService", ErrorMessagesEnum.GenericError, ex);
            }
        }


    }
}
