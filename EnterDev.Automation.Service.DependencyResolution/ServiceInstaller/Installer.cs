﻿namespace EnterDev.Automation.Service.DependencyResolution.ServiceInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using EnterDev.Automation.Domain.Interfaces.Service;
    using EnterDev.Automation.Service.Orchestrator;

    public class Installer : IWindsorInstaller
    {
        public Installer()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                 Component.For<IOrchestratorService>().ImplementedBy<OrchestratorService>().Named("OrchestratorService")
                    .LifestylePerWebRequest(),

                 Component.For<ISecurityService>().ImplementedBy<SecurityService>().Named("SecurityService")
                    .LifestylePerWebRequest()

                    );
        }
    }
}