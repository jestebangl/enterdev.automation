﻿namespace EnterDev.Automation.Service.DependencyResolution.InfraestructureInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using EnterDev.Automation.Domain.Interfaces.Infraestructure;
    using EnterDev.Automation.Infraestructure.ServiceIntegration.RestService;

    public class Installer : IWindsorInstaller
    {
        public Installer()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                 Component.For<IRestServiceIntegration>().ImplementedBy<RestServiceIntegration>().Named("RestServiceIntegration")
                    .LifestylePerWebRequest()

                    );
        }
    }
}