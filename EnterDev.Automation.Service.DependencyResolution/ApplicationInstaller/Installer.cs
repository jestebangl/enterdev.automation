﻿namespace EnterDev.Automation.Service.DependencyResolution.ApplicationInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using global::EnterDev.Automation.Application.Operator.Application;
    using global::EnterDev.Automation.Application.Operator.Helpers;
    using global::EnterDev.Automation.Domain.Interfaces.Application;

    public class Installer : IWindsorInstaller
    {
        public Installer()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                 Component.For<IUrlMakerHelper>().ImplementedBy<UrlMakerHelper>().Named("UrlMakerHelper")
                    .LifestylePerWebRequest(),

                 Component.For<IXmlHelper>().ImplementedBy<XmlHelper>().Named("XmlHelper")
                    .LifestylePerWebRequest(),

                 Component.For<IMachineApplication>().ImplementedBy<MachineApplication>().Named("MachineApplication")
                    .LifestylePerWebRequest(),

                 Component.For<ISecurityApplication>().ImplementedBy<SecurityApplication>().Named("SecurityApplication")
                    .LifestylePerWebRequest()

                    );
        }
    }
}