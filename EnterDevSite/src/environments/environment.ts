declare var data: any;

export const environment = {
  production: false,
  GetGroupModelFromService: data.basePathWebApi + 'api/group/getgroupmodelfromservice/',
  ExecuteAuthenticationService: data.basePathWebApi + 'api/security/executeauthenticationservice/',
  GetAutoData: data.basePathWebApi + 'api/security/getautodata/'
};

