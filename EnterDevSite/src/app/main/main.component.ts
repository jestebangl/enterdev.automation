import { Component, OnInit, ViewChild } from '@angular/core';
import { MachineComponent } from '../machine/machine.component';
import { SecurityComponent } from '../security/security.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  @ViewChild('machineComponent') machineComponent: MachineComponent;
  @ViewChild('securityComponent') securityComponent: SecurityComponent;

  constructor() { }

  ngOnInit() {
  }

}
