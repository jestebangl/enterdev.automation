import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components Import
import { MainComponent } from './main/main.component';
import { AppComponent } from './app.component';

const appRoutes: Routes = [
    {path: 'Main', component: MainComponent},
    {path: '', component: MainComponent},
    {path: '**', component: MainComponent}

];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
