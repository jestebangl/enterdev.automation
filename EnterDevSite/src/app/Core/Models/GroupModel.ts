import { AutoModel } from './AutoModel';

export class GroupModel {
    public name: string;

    public cid: string;

    public gid: string;

    public autoModelList: AutoModel[];

    constructor() { }
}

