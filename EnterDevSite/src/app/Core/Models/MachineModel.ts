import { GroupModel } from './GroupModel';

export class MachineModel {

    public id: string;
    public isTraining: string;
    public lastUpdate: string;
    public cid: string;
    public groupModelList: GroupModel[];

    constructor() {}
}
