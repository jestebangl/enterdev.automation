export class AutoModel {

    public name: string;
    public version: string;
    public fileName: string;
    public autoId: string;
    public realtime: string;
    public hash: string;
    public hidden: string;
    public key: string;
    public url: string;
    public secure: string;

    public id: string;
    public automation_file: string;
    constructor() {}

}
