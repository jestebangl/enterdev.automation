import { AutoModel } from './AutoModel';

export class SecurityModel {
    public success: string;
    public error: string;
    public id: string;
    public name: string;
    public owner: string;
    public token: string;
    public perms: string;
    public autos: AutoModel[];

    constructor () {}
}
