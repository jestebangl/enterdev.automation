import { Component, OnInit } from '@angular/core';
import { MachineModel } from '../Core/Models/MachineModel';
import { ConectorApiHelper } from '../Core/Helper/conectorApiHelper';
import { environment } from 'src/environments/environment';
import { ToasterService } from 'angular2-toaster';
import { GroupModel } from '../Core/Models/GroupModel';
import { AutoModel } from '../Core/Models/AutoModel';
import { TreeViewConfig } from '../Core/Helper/TreeViewConfig';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.css']
})
export class MachineComponent implements OnInit {

  public loading = false;
  public machineModel: MachineModel;
  public treeViewItems: any = [];
  public treeViewConfig = TreeViewConfig;

  private conectorApiHelper: ConectorApiHelper;
  private toasterService: ToasterService;

  constructor(conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService) {
    this.conectorApiHelper = conectorApiHelper;
    this.toasterService = toasterService;
    this.treeViewConfig = TreeViewConfig;
    this.GetGroupModelFromService();
  }

  ngOnInit() {
  }

  public GetGroupModelFromService() {
    this.loading = true;
    this.conectorApiHelper.ResolverPeticionGet(environment.GetGroupModelFromService).then(response => {
      this.machineModel = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      this.ContructTreeView(this.machineModel.groupModelList);
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });
  }

  private ContructTreeView(groupModelList: GroupModel[]) {
    for (let i = 0; i < groupModelList.length; i++) {
      const group = {
        text: groupModelList[i].name,
        value: groupModelList[i].cid + '-' +  groupModelList[i].gid,
        children: this.ConstructChildren(groupModelList[i].autoModelList)
      };

      this.treeViewItems.push(group);
    }
  }

  private ConstructChildren(autoModelList: AutoModel[]) {
    const children = [];
    for (let i = 0; i < autoModelList.length; i++) {
      const child = {
        text: autoModelList[i].name,
        value: autoModelList[i].autoId
      };

      children.push(child);
    }

    return children;
  }
}
