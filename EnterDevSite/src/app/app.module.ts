import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { ConectorApiHelper } from './Core/Helper/conectorApiHelper';
import { appRoutingProviders, routing } from './app-routing';
import { HttpModule } from '@angular/http';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { MachineComponent } from './machine/machine.component';
import { SecurityComponent } from './security/security.component';
import { TreeviewModule } from 'ngx-treeview';
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    MachineComponent,
    SecurityComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    ToasterModule.forRoot(),
    TreeviewModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [appRoutingProviders, ConectorApiHelper],
  bootstrap: [AppComponent]
})
export class AppModule { }
