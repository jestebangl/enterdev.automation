import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ConectorApiHelper } from '../Core/Helper/conectorApiHelper';
import { SecurityModel } from '../Core/Models/SecurityModel';
import { environment } from 'src/environments/environment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {

  public loading = false;
  public securityModel: SecurityModel;

  private conectorApiHelper: ConectorApiHelper;
  private toasterService: ToasterService;

  modalRef: BsModalRef;

  public dataAuto = '';

  constructor(conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService,
    private modalService: BsModalService) {
      this.conectorApiHelper = conectorApiHelper;
      this.toasterService = toasterService;

      this.ExecuteAuthenticationService();
    }

  ngOnInit() {
  }

  private ExecuteAuthenticationService() {
    this.loading = true;
    this.conectorApiHelper.ResolverPeticionGet(environment.ExecuteAuthenticationService).then(response => {
      this.securityModel = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });
  }

  public GetAutoInformation(template: TemplateRef<any>, idAuto) {
    this.loading = true;

    const model = {
      autoId: idAuto,
      token: this.securityModel.token
    };

    this.conectorApiHelper.ResolverPeticionPost(environment.GetAutoData, model).then(response => {
      this.dataAuto = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      this.modalRef = this.modalService.show(template);
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    });
  }

}
