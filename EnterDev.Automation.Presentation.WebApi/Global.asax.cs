﻿using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using EnterDev.Automation.Presentation.WebApi.Windsor;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace EnterDev.Automation.Presentation.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeIoC(GlobalConfiguration.Configuration);
        }

        public static void InitializeIoC(HttpConfiguration configuration)
        {
            var windsor = Ragolo.Core.IoC.IocHelper.Instance;
            var contenedor = windsor.GetContainer();

            windsor.Install(new Service.DependencyResolution.InfraestructureInstaller.Installer());

            windsor.Install(new Service.DependencyResolution.ApplicationInstaller.Installer());

            windsor.Install(new Service.DependencyResolution.ServiceInstaller.Installer());

            windsor.Install(new Windsor.Installer.Installer());

            WindsorControllerFactory controllerFactory = new WindsorControllerFactory(contenedor.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            contenedor.Kernel.Resolver.AddSubResolver(new CollectionResolver(contenedor.Kernel, true));
            contenedor.BeginScope();
            var dependencyResolver = new WindsorDependencyResolver(contenedor);
            configuration.DependencyResolver = dependencyResolver;
        }
    }
}
