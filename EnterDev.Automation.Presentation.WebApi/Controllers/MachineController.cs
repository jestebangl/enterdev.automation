﻿namespace EnterDev.Automation.Presentation.WebApi.Controllers
{
    using EnterDev.Automation.Domain.Interfaces.Service;
    using System.Web.Http;

    public class MachineController : ApiController
    {
        public IOrchestratorService OrchestratorService { get; set; }

        public MachineController(IOrchestratorService OrchestratorService)
        {
            this.OrchestratorService = OrchestratorService;
        }

        [HttpGet]
        [Route("api/group/getgroupmodelfromservice/")]
        public IHttpActionResult GetGroupModelFromService()
        {
            return Ok(OrchestratorService.GetMachineFromService());
        }

        [HttpGet]
        [Route("api/group/getdownloaddocuments/")]
        public IHttpActionResult GetDownloadDocuments()
        {
            return Ok(OrchestratorService.GetMachineFromService());
        }
    }
}
