﻿namespace EnterDev.Automation.Presentation.WebApi.Controllers
{
    using EnterDev.Automation.Domain.Entities.Model;
    using EnterDev.Automation.Domain.Interfaces.Service;
    using System.Web.Http;

    public class SecurityController : ApiController
    {
        public ISecurityService SecurityService { get; set; }

        public SecurityController(ISecurityService SecurityService)
        {
            this.SecurityService = SecurityService;
        }

        [HttpGet]
        [Route("api/security/executeauthenticationservice/")]
        public IHttpActionResult ExecuteAuthenticationService()
        {
            return Ok(SecurityService.ExecuteAuthenticationService());
        }

        [HttpPost]
        [Route("api/security/getautodata/")]
        public IHttpActionResult GetAutoData(AutoAuthorizationModel autoAthorizationModel)
        {
            return Ok(SecurityService.GetAutoData(autoAthorizationModel));
        }
    }
}
